import React, { useEffect, useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Grid from '@mui/material/Grid';
import Head from 'next/head'
import Link from 'next/link'
import styles from '../components/layout.module.css'
import TextField from '@mui/material/TextField';
import WorldCard from '../components/WorldCard';

const Explore = () => {
  const siteTitle = "Explore Worlds"
  const [filter, setFilter] = useState([]);
  const [worlds, setWorlds] = useState([]);


  // Get Worlds
  useEffect(() => {
    fetch("/api/world/public")
    .then(response => response.json())
    .then(data => {
      setFilter(data)
      setWorlds(data)
    });
  }, []);

  // Filter Results Based on Search Input
  function searchResults(value) {
    setFilter([])
    const length = worlds.length;
    const array = []
    const v = String(value).toLowerCase()
    for (let i = 0; i < length; i++) {
      const w = worlds[i]
      const prompt = String(w.Prompt).toLowerCase()
      const title = String(w.Title).toLowerCase()
      if (title.includes(value)){
        array.push(w);
        continue
      }
      else if (prompt.includes(value)){
        array.push(w);
        continue
      }
    }
    setFilter(array)
  }


  return (
    <div>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <center>
        <h1>Welcome to Immria</h1>
        <p style={{ fontSize: "25px" }}>Hangout in beautiful 3D worlds that you create in just a couple of words</p>
        <Link href="/create"><Button variant="contained">Create World</Button></Link>
        <br/>
        <h2>Join World</h2>
        { (worlds.length > 0)
          ? 
          <div>
            <Box sx={{ width: 600 }}>
              <TextField id="filled-basic" label="Search for world based on name or attributes..." onChange={(e) => searchResults(e.target.value)} variant="filled" fullWidth/>
            </Box>
            <br/>
            <br/>
            <Grid container spacing={2}>
              {filter.map((world) => (
                <Grid item key={world._id} xs={12} sm={6} md={4} lg={3}>
                  <WorldCard world={world}/>
                </Grid>
              ))}
            </Grid>
          </div>
          :
          <div>
            <CircularProgress />
          </div>
        }
        <div className={styles.backToHome}>
          <Link href="/">← Back to home</Link>
        </div>
        <br/>
      </center>

    </div>
  )
}

export default Explore