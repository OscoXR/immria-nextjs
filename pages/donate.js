import { FORM_URL, FEATURES_COMPLETE, FEATURES_UPCOMING } from '../lib/config'
import Head from 'next/head'
import Link from 'next/link';
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'

export default function Donate({}) {
  // Allow this component to access our user state
  // const [user] = useContext(UserContext);
  const siteTitle = "Donate"

  return (
    <Layout>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h4>{siteTitle}</h4>
        <h5>Here is a list of upcoming features:</h5>
      </section>
    </Layout>
  )
}