// This is just a wrapper to get the styles into the create.js component

// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import clientPromise from '../../../lib/mongodb'
import { ObjectId } from 'mongodb'
import { BlockadeLabsSdk } from '@blockadelabs/sdk';
if (!process.env.BLOCKADE_LABS_API_KEY) {
  throw new Error('Invalid/Missing environment variable: "BLOCKADE_LABS_API_KEY"')
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Connect to Blockade Labs SDK
  const API_KEY = process.env.BLOCKADE_LABS_API_KEY
  const sdk = new BlockadeLabsSdk({
    api_key: API_KEY, // REQUIRED
  });

  // Generate Skybox Status Based on Request Body
  const styles = await sdk.getSkyboxStyles();
  console.log(styles)
  res.status(200).json(styles);
}

  