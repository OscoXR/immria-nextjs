// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import clientPromise from '../../../lib/mongodb'
import { ObjectId } from 'mongodb'

// `await clientPromise` will use the default database passed in the MONGODB_URI
// However you can use another database (e.g. myDatabase) by replacing the `await clientPromise` with the following code:
//
// `const client = await clientPromise`
// `const db = client.db("myDatabase")`
//
// Then you can execute queries against your database like so:
// db.find({}) or any of the MongoDB Node Driver commands

// type Data = {
//   name: string
// }

export default async function handler(
  req,
  res,
) {
  // Connect to Database
  const client = await clientPromise;
  const db = client.db("Myujen");
  let collection = await db.collection("Worlds");

  // Create Query and Loop Over All Documents
  let result = [];
  // let query = { "Status": 'complete' }
  let query = {}
  for await (const doc of collection.find(query)) {
    result.push(doc)
  }
  if (!result) res.status(404).json({"result": "not found"})
  else res.status(200).json(result);
}

  