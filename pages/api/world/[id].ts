// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import clientPromise from '../../../lib/mongodb'
import { ObjectId } from 'mongodb'
import Cors from 'cors'

// `await clientPromise` will use the default database passed in the MONGODB_URI
// However you can use another database (e.g. myDatabase) by replacing the `await clientPromise` with the following code:
//
// `const client = await clientPromise`
// `const db = client.db("myDatabase")`
//
// Then you can execute queries against your database like so:
// db.find({}) or any of the MongoDB Node Driver commands

// type Data = {
//   name: string
// }

// Initializing the cors middleware
// You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
const cors = Cors({
  methods: ['POST', 'GET', 'HEAD'],
})

// Helper method to wait for a middleware to execute before continuing
// And to throw an error when an error happens in a middleware
function runMiddleware(
  req: NextApiRequest,
  res: NextApiResponse,
  fn: Function
) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result: any) => {
      if (result instanceof Error) {
        return reject(result)
      }

      return resolve(result)
    })
  })
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Run the middleware
  await runMiddleware(req, res, cors)

  // Parse Request
  const { query } = req
  const { id } = query

  // Query Database
  const client = await clientPromise;
  const db = client.db("Myujen");
  let collection = await db.collection("Worlds");
  // let query = {_id: ObjectId(req.params.id)};
  // let dbQuery = {_id: ObjectId("644e7bb0ceb7f6e36b573667")};
  let dbQuery = {_id: new ObjectId(id as string)};
  let result = await collection.findOne(dbQuery);
  if (!result) res.status(404).json({"result": "not found"})
  else res.status(200).json(result);
}

