# API - World
## Notes
* [id].ts has a CORS header and simply returns the world as is in the DB
* get.ts checks if the status of the document is TBD, then checks blockade labs sdk, then resizes image, uploads to s3, and updates mongodb document