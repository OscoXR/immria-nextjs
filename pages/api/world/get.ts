// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import clientPromise from '../../../lib/mongodb'
import Jimp from "jimp";
import { ObjectId } from 'mongodb'
import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";
import { BlockadeLabsSdk } from '@blockadelabs/sdk';
if (!process.env.BLOCKADE_LABS_API_KEY) {
  throw new Error('Invalid/Missing environment variable: "BLOCKADE_LABS_API_KEY"')
}


// `await clientPromise` will use the default database passed in the MONGODB_URI
// However you can use another database (e.g. myDatabase) by replacing the `await clientPromise` with the following code:
//
// `const client = await clientPromise`
// `const db = client.db("myDatabase")`
//
// Then you can execute queries against your database like so:
// db.find({}) or any of the MongoDB Node Driver commands

// type Data = {
//   name: string
// }

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  // Parse Request
  const { query } = req
  const { id } = query

  // Query Database
  const client = await clientPromise;
  const db = client.db("Myujen");
  let collection = await db.collection("Worlds");
  // let query = {_id: ObjectId(req.params.id)};
  // let dbQuery = {_id: ObjectId("644e7bb0ceb7f6e36b573667")};
  let dbQuery = {_id: new ObjectId(id as string)};
  let result = await collection.findOne(dbQuery);
  if (!result) {
    res.status(404).json({"result": "not found"})
  } else {
    
    // If value is TBD, check SDK for an update
    if (result.ThumbUrl === "TBD") {
      // Connect to Blockade Labs SDK
      const API_KEY = process.env.BLOCKADE_LABS_API_KEY
      const sdk = new BlockadeLabsSdk({
        api_key: API_KEY, // REQUIRED
      });

      // Generate Skybox Status Based on Request Body
      const skyboxId = result.SkyboxId
      console.log("[api/world/get] skybox id: "+ skyboxId)
      const imagineResult = await sdk.getImagineById({
        id: skyboxId, // REQUIRED
      });
      console.log("---imagineResult: ")
      console.log(imagineResult)

      // If Skybox Generation is Complete, Resize Image and Update MongoDB
      if (imagineResult.status === "complete"){
        const imageUrl = imagineResult.file_url
        const thumbUrl = imagineResult.thumb_url
        console.log("imageUrl: " + imageUrl)
        console.log("thumbUrl:" + thumbUrl)

        // -- Resize Image and Upload to S3
        console.log("resizing image for s3 upload...")

        // Read the image from a URL
        const image = await Jimp.read(imageUrl);

        // Resize the image
        const resizedImage = await image.resize(4096, 2048);

        // Reduce the quality of the image
        // -- NOTES -- //
        // -- Quality 100 = ~3.3mb file
        // -- Quality 85 = ~600kb file
        const lowQualityImage = await resizedImage.quality(85);

        // Convert the image to a Buffer
        const buffer = await lowQualityImage.getBufferAsync(Jimp.MIME_JPEG);
        console.log("resize complete!")


        // Set S3 Client Credentials
        console.log("---starting aws upload....")
        const s3 = new S3Client({
          region: process.env.AWS_REGION,
          credentials:{
              accessKeyId: process.env.AWS_ACCESS_KEY_ID,
              secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
          }
        });

        // Set Put Command
        const command = new PutObjectCommand({
          Bucket: process.env.AWS_BUCKET, 
          Key: `skyboxes/${id}.jpg`,
          Body: buffer,
          ContentType: 'image/jpg',
          ContentEncoding: 'base64'
        });

        // Upload to S3
        try {
          const response = await s3.send(command);
          console.log(response);
        } catch (err) {
          console.error(err);
        }
        console.log("---finish aws upload!")

        // -- Update MongoDb Entry -- //
        const resizedUrl = `https://myujen.s3.us-east-2.amazonaws.com/skyboxes/${id}.jpg`

        // Create a filter for a document to update
        const filter = {_id: new ObjectId(id as string)};

        // this option instructs the method to , if true, create a document if no documents match the filter
        const options = { upsert: false };

        // create a document that sets the plot of the movie
        const updateDoc = {
          $set: {
            "ImageUrl": imageUrl,
            "ResizedUrl": resizedUrl,
            "Status": "complete",
            "ThumbUrl": thumbUrl,
          },
        };

        // Update Database
        const resultUpdate = await collection.updateOne(filter, updateDoc, options);
        console.log(resultUpdate);

        // Return updated value! 
        result.ImageUrl = imageUrl
        result.ResizedUrl = resizedUrl
        result.ThumbUrl =  thumbUrl

        // Return Updated Result
        res.status(200).json(result);
      } else {
        // Return loading :( 
        res.status(200).json(result);
      }
    } else {
    // Else return value as is!
    res.status(200).json(result);
    }
  }
}

  
