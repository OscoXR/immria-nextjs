// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import Jimp from "jimp";
import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";
import clientPromise from '../../../lib/mongodb'
import { ObjectId } from 'mongodb'

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  // -- Parse Req Body -- //
  // Parse Request Body
  const { body } = req
  const { 
    id,
    imageUrl
  } = body  

  // Read the image from a URL
  const image = await Jimp.read(imageUrl);

  // Resize the image
  const resizedImage = await image.resize(4096, 2048);

  // Reduce the quality of the image
  // -- NOTES -- //
  // -- Quality 100 = 3.3mb file
  // -- Quality 85 = 600kb file
  const lowQualityImage = await resizedImage.quality(85);

  // Convert the image to a Buffer
  const buffer = await lowQualityImage.getBufferAsync(Jimp.MIME_JPEG);

  // Set S3 Client Credentials
  console.log("starting aws upload....")
  const s3 = new S3Client({
    region:'us-east-2', // TODO: Replace with ENV VAR
    credentials:{
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    }
  });

  // Set Put Command
  const command = new PutObjectCommand({
    Bucket: "myujen", // TODO: Replace with ENV VAR
    Key: `skyboxes/${id}.jpg`, // TODO: Replace with Request Body
    Body: buffer,
    ContentType: 'image/jpg',
    ContentEncoding: 'base64'
  });

  // Upload to S3
  try {
    const response = await s3.send(command);
    console.log(response);
  } catch (err) {
    console.error(err);
  }
  console.log("finish aws upload")

  // -- Update MongoDb Entry -- //
  const resizedUrl = `https://myujen.s3.us-east-2.amazonaws.com/skyboxes/${id}.jpg`

  // Connect to Database
  console.log("Update Database Entry")
  const client = await clientPromise;
  const db = client.db("Myujen");
  let collection = await db.collection("Worlds");

  // Create a filter for a movie to update
  const filter = {_id: new ObjectId(id as string)};

  // this option instructs the method to, if true, create a document if no documents match the filter
  const options = { upsert: false };

  // Create New Field for ResizedImage
  const updateDoc = {
    $set: {
      "ResizedUrl": resizedUrl,
    },
  };

  const result = await collection.updateOne(filter, updateDoc, options);
  console.log(result)
  res.status(200).json(result)
}
