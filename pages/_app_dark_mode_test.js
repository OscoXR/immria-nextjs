import '../styles/global.css'
import { createContext, useContext, useEffect, useMemo, useState } from 'react';
import { magic } from '../lib/magic';
import { UserContext } from '../lib/UserContext';
import NavBar from '../components/NavBar';
import { useTheme, ThemeProvider, createTheme } from '@mui/material/styles';
// import { useRouter } from 'next/router';
import Brightness4Icon from '@mui/icons-material/Brightness4';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import IconButton from '@mui/material/IconButton';

const ColorModeContext = createContext({ toggleColorMode: () => {} });

function MyApp({ Component, pageProps }) {
  const colorMode = useContext(ColorModeContext);
  const theme = useTheme();
  // const router = useRouter();  
  const [user, setUser] = useState();
  
  useEffect(() => {
    // Set loading to true to display our loading message within pages/index.js
    setUser({ loading: true });
    // Check if the user is authenticated already
    magic.user.isLoggedIn().then((isLoggedIn) => {
      if (isLoggedIn) {
        // Pull their metadata, update our state, and route to dashboard
        magic.user.getMetadata().then((userData) => setUser(userData));
        // router.push('/dashboard');
      } else {
        // If false, route them to the login page and reset the user state
        // router.push('/login');
        setUser({ user: null });
      }
    });
    // Add an empty dependency array so the useEffect only runs once upon page load
  }, []);

  return(
    <UserContext.Provider value={[user, setUser]}>

        {/* TEST ONLY */}
        {theme.palette.mode} mode
      <IconButton sx={{ ml: 1 }} onClick={colorMode.toggleColorMode} color="inherit">
        {theme.palette.mode === 'dark' ? <Brightness7Icon /> : <Brightness4Icon />}
      </IconButton>

      <NavBar/>
      <Component {...pageProps} />
    </UserContext.Provider>
  )
}

export default function App({ Component, pageProps }) {
  const [mode, setMode] = useState('dark');
  const colorMode = useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    [],
  );

  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode],
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
      <ThemeProvider theme={theme}>
        <MyApp Component={Component} pageProps={pageProps}/>
      </ThemeProvider>
    </ColorModeContext.Provider>
  );
}