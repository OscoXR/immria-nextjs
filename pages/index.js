import React from 'react'
import Image from 'next/image'
import Link from 'next/link';

const index = () => {
  return (
    <main>
      <center>
        <h1>This page is under development!</h1> 
        <h2>
          <Link href={"https://x.com/Atemosta/status/1621273399250096128?s=20"}>
            But you can check out a demo of the latest version here.
            <br/>
            <br/>
            <Image
              priority
              src="/images/sinon.gif"
              height={570}
              width={500}
              alt={"support"}
            />
          </Link>
        </h2>  
      </center>
    </main>
  )
}

export default index