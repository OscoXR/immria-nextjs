import { FORM_URL, FEATURES_COMPLETE, FEATURES_UPCOMING } from '../lib/config'
import Head from 'next/head'
import Link from 'next/link';
import Layout from '../components/layout'
import utilStyles from '../styles/utils.module.css'

export default function Roadmap({}) {
  // Allow this component to access our user state
  // const [user] = useContext(UserContext);
  const siteTitle = "Roadmap"

  return (
    <Layout>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <section className={utilStyles.headingMd}>
        <h4>{siteTitle}</h4>
        <h5>Here is a list of upcoming features:</h5>
        <ul>
          {FEATURES_UPCOMING.map(({feature, priority}) => (
            <li>{feature}: <i>Priority - <b>{priority}</b></i></li>
          ))}
        </ul>
        <h5>You can vote for or suggest news ones through <Link href={FORM_URL}>this form</Link></h5>

        <h5>Completed features</h5>
        <ul>
          {FEATURES_COMPLETE.map(({feature, date}) => (
            <li>{feature} - <i>{date}</i></li>
          ))}
        </ul>
      </section>
    </Layout>
  )
}