import { useContext, useEffect } from 'react';
import { UserContext } from '../lib/UserContext';
import { magic } from '../lib/magic';
import { useRouter } from 'next/router';
import Button from '@mui/material/Button';
import Head from 'next/head'
import Layout from '../components/layout'

export default function Profile() {
  const [user, setUser] = useContext(UserContext);
  // Create our router
  const router = useRouter();
  const siteTitle = "Profile"

  useEffect(() => {
    // Route to login page if no user
    !(user?.issuer) && router.push('/login');
  }, [user]);  

  const logout = () => {
    // Call Magic's logout method, reset the user state, and route to the login page
    magic.user.logout().then(() => {
      setUser({ user: null });
      router.push('/');
    });  
  };

  return (
    <Layout>
    <Head>
      <title>{siteTitle}</title>
    </Head>
      {/* We check here to make sure user exists before attempting to access its data */}
      {user?.issuer && (
        <>
          <center>
          <h1>Profile</h1>
          <h2>Email</h2>
          <p>{user.email}</p>
          {/* <h2>Wallet Address</h2>
          <p>{user.publicAddress}</p> */}
          <Button variant="contained" onClick={logout}>Logout</Button>
          </center>
        </>
      )}
    </Layout>
  );
}