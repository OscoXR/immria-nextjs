import React, { useEffect, useState, useContext } from 'react';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import Head from 'next/head';
import Image from 'next/image';
import Layout from '../../../components/layout';
import Link from 'next/link';
import TextField from '@mui/material/TextField';
import { UserContext } from '../../../lib/UserContext'
import { useRouter } from 'next/router';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import { PUSHER_CLUSTER, PUSHER_KEY, STYLES } from '../../../lib/config';
import Pusher from 'pusher-js'
const pusher = new Pusher(PUSHER_KEY, {
  cluster: PUSHER_CLUSTER,
  encrypted: true
})

const pageName = "Remix - Create a New Version"
export default function Create() {
  const [email, setEmail] = useState(null)
  const [id, setId] = useState(null);
  const [loading, setLoading] = useState(false); 
  const [prompt, setPrompt] = useState(null); 
  const [submitted, setSubmitted] = useState(false);
  const [status, setStatus] = useState(null); 
  const [styleId, setStyleId] = useState(''); 
  const [title, setTitle] = useState(null); 
  const [url, setUrl] = useState(null);
  const [worldOG, setWorldOG] = useState(null); 

  // Allow this component to access our user state
  const [user] = useContext(UserContext);
  const router = useRouter();

  // Make sure to add useEffect to your imports at the top
  useEffect(() => {
    // Route to login page if no user
    !(user?.issuer) && router.push('/login');

    // Get world info
    // (async function() {
      const id = window.location.href.split('/').pop(); // get last part of url lolol
      setId(id)
      fetch("/api/world/get?id="+id)
      .then(response => response.json())
      .then(data => {
        setWorldOG(data);
        setTitle(data.Title)
        setPrompt(data.Prompt)
      });
    // })();
    
    // Set Email
    if (user){
      setEmail(user.email)
    }
  }, [user]);  


  const handleSelect = (event) => {
    const value = event.target.value;
    setStyleId(value);
  };


  // Generate New World with CreateWorld API
  const generateWorld = async () => {
    if (!prompt || !styleId || !title){ // TODO: Add Type when new worlds exist
      // if (!desc || !image || !title){
      alert("Please fill in all required fields!")
    }
    else {
      setLoading(true);
      // Submit Params to API
      // try {
        const world = {
          prompt: prompt, 
          remixId: worldOG.SkyboxId,
          skyboxStyleId: styleId,
          skyboxStyleName: STYLES[styleId].name,
          title: title,
          userEmail: email,
        }
        const response = await fetch("/api/world/remix", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(world),
        });
        const responseJson = await response.json(); 
        if (responseJson.insertedId){
          const worldId = responseJson.insertedId
          setSubmitted(true);
          setUrl("/world/" + worldId)

          // Set Pusher Channel
          const channel = pusher.subscribe(responseJson.pusherChannel)
          channel.bind(responseJson.pusherEvent, data => {
            console.log("status: " + data.status)
            setStatus(data.status)
          })
        }
      // } catch (error) {
      //   console.log("Error in remix/[id].ts");
      //   console.log(error);
      // }
      setLoading(false)
    }
  };

    return (
    <div>
      <Layout>  
        <Head>
          <title>{pageName}</title>
        </Head>
        <h1>{pageName}</h1>
        {!(user) && <p>Loading...</p>}
        {!(worldOG) && <p>Loading...</p>}
        { (worldOG && user) &&
          <div>
            {/* Show World Thumbnail */}
            <center>
              <Image
                src={worldOG.ImageUrl}
                width={500}
                height={250}
                alt="Picture of the author"
              />
            <br/>
            </center>
            {/* Show World Redirect If Successfully */}
            {(status === "complete") && 
              <div>
                <Alert severity="success">Successfully generated world! Check it out <Link href={url}>here</Link></Alert> 
                <br/>
                <br/>
              </div>
            }
            {/* Show Failure Alert for World Generation */}
            {(status === "abort" || status === "error") && 
              <div>
                <Alert severity="error">Unable to create world. Please refresh the page and try again.</Alert> 
                <br/>
                <br/>
              </div>
            }
            {/* Show In Progess Alert for World Generation */}
            {(status === "pending" || status === "dispatched" || status === "processing") && 
              <div>
                <Alert severity="info">World generation in progess...<br/><br/><b>Status</b>: <i>{status.toUpperCase()}</i> </Alert> 
                <br/>
                <br/>
              </div>
            }
            {/* Form.Email */}
            {/* <TextField
              disabled
              id="outlined-disabled"
              label="Email"
              defaultValue={email}
              fullWidth
            />
            <br/>
            <br/> */}
            {/* Form.Title */}
            <TextField id="outlined-id" fullWidth label="Name of the World" placeholder="What will this world be called?" defaultValue={worldOG.Title}  onChange={(e) => setTitle(e.target.value)} variant="outlined"/>
            <br/>
            <br/>
            {/* Form.Style */}
            <InputLabel id="demo-simple-select-label">Style</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={styleId}
              label="Style"
              onChange={handleSelect}
              defaultValue={worldOG.StyleId}
            >
              {STYLES.map((style) => (
                <MenuItem value={style.id} >{style.name}</MenuItem>
              ))}
            </Select>
            <br/>
            <br/>
            {/* Form.Prompt */}
            <TextField id="outlined-prompt" fullWidth label="Prompt" multiline placeholder="Describe your ideal virtual reality world~" defaultValue={worldOG.Prompt}  onChange={(e) => setPrompt(e.target.value)} rows={4} variant="outlined" />
            <br/>
            <br/>
            {/* Submit Button */}
            <Button variant="contained" onClick={() => generateWorld()} disabled={loading}>{(!loading) ? "Submit" : "Creating..."}</Button>
          </div>
        }
      </Layout>
      </div>
    );
  }
