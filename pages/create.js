import React, { useEffect, useState, useContext } from 'react';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';
import Head from 'next/head';
import Layout from '../components/layout';
import Link from 'next/link';
import TextField from '@mui/material/TextField';
import { UserContext } from '../lib/UserContext'
import { useRouter } from 'next/router';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Pusher from 'pusher-js'
import { PUSHER_CLUSTER, PUSHER_KEY, STYLES } from '../lib/config';
const pusher = new Pusher(PUSHER_KEY, {
  cluster: PUSHER_CLUSTER,
  encrypted: true
})
// const channel = pusher.subscribe('rotten-pepper')

const pageName = "Create Virtual Reality World"
export default function Create() {
  const [loading, setLoading] = useState(false); 
  const [prompt, setPrompt] = useState(null); 
  const [status, setStatus] = useState(null); 
  const [styleId, setStyleId] = useState(''); 
  const [title, setTitle] = useState(null); 
  const [world, setWorld] = useState(null);

  // Allow this component to access our user state
  const [user] = useContext(UserContext);
  const router = useRouter();

  // Make sure to add useEffect to your imports at the top
  useEffect(() => {
    // Route to login page if no user
    !(user?.issuer) && router.push('/login');
  }, [user]);  


  const handleSelect = (event) => {
    const value = event.target.value;
    setStyleId(value);
  };

  // Generate New World with CreateWorld API
  const generateWorld = async () => {
    if (!prompt || !styleId || !title){ // TODO: Add Type when new worlds exist
      // if (!desc || !image || !title){
      alert("Please fill in all required fields!")
    }
    else {
      setLoading(true);
      // Submit Params to API
      try {
        const world = {
          prompt: prompt, 
          skyboxStyleId: styleId,
          skyboxStyleName: STYLES[styleId].name,
          title: title,
          userEmail: user.email,
        }
        const response = await fetch("../api/world/create", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(world),
        });
        const responseJson = await response.json(); 
        if (responseJson.insertedId){
          // Set World Status
          const worldId = responseJson.insertedId
          setWorld("/world/" + worldId);

          // Set Pusher Channel
          const channel = pusher.subscribe(responseJson.pusherChannel)
          channel.bind(responseJson.pusherEvent, data => {
            console.log("status: " + data.status)
            setStatus(data.status)
          })
        }
      } catch (error) {
        console.log("Error in create.tsx");
        console.log(error);
      }
      setLoading(false)
    }
  };

    return (
    <div>
      <Layout>  
        <Head>
          <title>{pageName}</title>
        </Head>
        <h1>{pageName}</h1>
        {!(user) // If user is null 
          ?
          <p>Loading...</p>
          :
          <div>
            {(status === "complete") && 
              <div>
                <Alert severity="success">Successfully generated world! Check it out <Link href={world}>here</Link></Alert> 
                <br/>
                <br/>
              </div>
            }
            {/* Show Failure Alert for World Generation */}
            {(status === "abort" || status === "error") && 
              <div>
                <Alert severity="error">Unable to create world. Please refresh the page and try again.</Alert> 
                <br/>
                <br/>
              </div>
            }
            {/* Show In Progess Alert for World Generation */}
            {(status === "pending" || status === "dispatched" || status === "processing") && 
              <div>
                <Alert severity="info">World generation in progess...<br/><br/><b>Status</b>: <i>{status.toUpperCase()}</i> </Alert> 
                <br/>
                <br/>
              </div>
            }
            {/* Form.Email */}
            <TextField
              disabled
              id="outlined-disabled"
              label="Email"
              defaultValue={user.email}
              fullWidth
            />
            <br/>
            <br/>
            {/* Form.Title */}
            <TextField id="outlined-id" fullWidth label="Name of the World" placeholder="What will this world be called?"  onChange={(e) => setTitle(e.target.value)} variant="outlined"/>
            <br/>
            <br/>
            {/* Form.Style */}
            <InputLabel id="demo-simple-select-label">Style</InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={styleId}
              label="Style"
              onChange={handleSelect}
            >
              {STYLES.map((style) => (
                <MenuItem value={style.id} >{style.name}</MenuItem>
              ))}
            </Select>
            <br/>
            <br/>
            {/* Form.Prompt */}
            <TextField id="outlined-prompt" fullWidth label="Prompt" multiline placeholder="Describe your ideal virtual reality world"  onChange={(e) => setPrompt(e.target.value)} rows={4} variant="outlined" />
            <br/>
            <br/>
            {/* Submit Button */}
            <Button variant="contained" onClick={() => generateWorld()} disabled={loading}>{(!loading) ? "Submit" : "Creating..."}</Button>
          </div>
        }
      </Layout>
      </div>
    );
  }
