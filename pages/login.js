import { magic } from '../lib/magic';
import { useContext, useState, useEffect } from 'react';
import { UserContext } from '../lib/UserContext';
import { useRouter } from 'next/router'
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import Head from 'next/head'
import Layout from '../components/layout'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';

export default function SignIn() {
  const [user, setUser] = useContext(UserContext);   // Allows us to access the user state and update it within this page
  const [email, setEmail] = useState('');   // Our email state that we'll use for our login form
  // Create our router
  const router = useRouter();
  const siteTitle = "Login"

  // Make sure to add useEffect to your imports at the top
  useEffect(() => {
    // Check for an issuer on our user object. If it exists, route them to last module they were at.
    user?.issuer && router.back();
  }, [user]);

  const handleLogin = async (e) => {
    e.preventDefault();
    
    // Log in using our email with Magic and store the returned DID token in a variable
    try {
      const didToken = await magic.auth.loginWithMagicLink({
        email,
      });

      // Send this token to our validation endpoint
      const res = await fetch('/api/login', {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
          Authorization: `Bearer ${didToken}`,
        },
      });
      // If successful, add to our database, update our user state with their metadata, and route to the last module they were at
      if (res.ok) {
        // Add User to Database if they do not exist
        console.log("--Adding user to DB...")
        console.log("email: " + email)
        await fetch("../api/user/create", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({"email": email}),
        });
        console.log("--Adding user complete!")
        
        //  update our user state with their metadata,
        const userMetadata = await magic.user.getMetadata();
        setUser(userMetadata);

        // route to the last module they were at
        router.back();
      }
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <Layout>
      <Head>
        <title>{siteTitle}</title>
      </Head>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 0,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'orange' }}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Create Account or Sign In
          </Typography>
          <Box component="form" onSubmit={handleLogin} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
              onChange={(e) => setEmail(e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Send Magic Link
            </Button>
          </Box>
        </Box>
      </Container>
    </Layout>
  );
}