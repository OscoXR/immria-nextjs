# Eff You Mongo DB for making this difficult lol

# import necessary libraries from creds
import datetime
from creds import MONGODB_URI
import json
from pymongo import MongoClient

# name of file for this version of backup
todays_date = datetime.datetime.now().strftime('%Y-%m-%d')
file_name = f'backup/myujen-users-{todays_date}.json'

# create client with MONGODB_URI
client = MongoClient(MONGODB_URI)
# get the Myujen database
db = client.Myujen
# find all users in Users collection
data = db.Users.find()

# initialize count and array
count = 0
j = []
# loop through users and convert ObjectId to string
for d in data:
  objId = d["_id"]
  d["_id"] = str(objId)
  j.append(d)
  count+=1

# write array to JSON file
with open(file_name, 'w') as outfile:
  json.dump(list(j), outfile)

# print confirmation that backup is complete
print("User Backup Complete! Users: " + str(count))