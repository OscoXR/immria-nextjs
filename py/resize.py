### --- Resizes All Current Worlds via Resize API
import asyncio 
import json
import requests

## -- Start Process
print("--hewwo owo")

## -- Set Url
url = "http://localhost:3000/api/world/public"

## -- Get All Worlds via API
print("--Getting worlds...")
payload = {}
headers = {}
response = requests.request("GET", url, headers=headers, data=payload)
worlds = json.loads(response.text)
print("--Got all worlds!")

## -- Use Resize API on Each World
for world in worlds:
  urlResize = "http://localhost:3000/api/world/resize"
  id = world["_id"]
  print(f"--updating {id}...")
  payload = json.dumps({
    "id": id,
    "imageUrl": world["ImageUrl"]
  })
  headers = {
    'Content-Type': 'application/json'
  }
  r = requests.request("POST", urlResize, headers=headers, data=payload)
  print("Status Code: " + str(r.status_code))
